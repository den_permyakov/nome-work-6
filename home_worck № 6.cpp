﻿#include <iostream>
#include <string>

using namespace std;

class player // создание  игрового класса
{
private:
	string namePlayer;
	int scorePlayer;

public:
	player(): namePlayer("Un"), scorePlayer(0)
	{}

	player(string _namePlayer, int _scorePlayer):
		namePlayer(_namePlayer), scorePlayer(_scorePlayer)
	{}
	

	void Snow()
	{
		cout << '\n' << namePlayer << ":"<< scorePlayer;
	}

	int getScorePlayer()
	{
		return scorePlayer;
	}

	void setNamePlayer(string newNamePlayer)
	{
		namePlayer = newNamePlayer;
	}

	void setPlayer(string newNamePlayer, int newScorePlayer)
	{
		namePlayer = newNamePlayer;
		scorePlayer = newScorePlayer;
	}
	
};

void SelectionSort(player* base, int size)        //сортировка  игроков
{
	int j;
	player tmp;
	for (int i = 0; i < size; i++) {
		j = i;
		for (int k = i; k < size; k++) {
			if (base[j].getScorePlayer() > base[k].getScorePlayer()) {
				j = k;
			}

		}
    	
		tmp = base[i];
		base[i] = base[j];
		base[j] = tmp;
	}
	
	

}
int main()
{
	cout << "How many players do you want?\n";

	int size;
	cin >> size;

	string arrName;
	int arrScore;

	player* base = new player[size];


	for (int i = 0, j = 0; i < size; i++, j++)
	{
		string y;
		int x;

		cout << "Enter" << i + 1 << "name:";
		cin >> y;
		arrName = y;

		cout << "Enter" << y << "score:";
		cin >> x;
		arrScore = x;

		base[i].setPlayer(arrName, arrScore);
	}
	
	cout << "\nYou enter:\n";
	for (int i = 0; i < size; i++) 
	{
		base[i].Snow();
	}

	SelectionSort(base, size);

	cout << endl;

	for(int i=0;i<size;i++)
	{
		base[i].Snow();
	}

	return 0;
}	